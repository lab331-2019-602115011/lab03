import { TestBed } from '@angular/core/testing';

import { StudentsProflieImplService } from './students-proflie-impl.service';

describe('StudentsProflieImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentsProflieImplService = TestBed.get(StudentsProflieImplService);
    expect(service).toBeTruthy();
  });
});
